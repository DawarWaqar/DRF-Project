from basic.models import Posts, Comments
from rest_framework import serializers

#model serializer 

class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comments
        read_only_fields = ['post']
        fields = "__all__"


class PostsSerializer(serializers.ModelSerializer):
    # comments = serializers.StringRelatedField(many=True)
    comments = serializers.SlugRelatedField(many=True, read_only=True, slug_field='content')
   
    class Meta:
        model = Posts
        fields = "__all__"

    def validate_content(self, value):
       
        errors = []

        if len(value) > 100:
            errors.append(serializers.ValidationError(
                'Len greater than 100'
            ))

        # check digit
        for char in value:
            if char.isdigit():
                errors.append(serializers.ValidationError(
                'Error message'))
                break
            
        if errors:
            raise serializers.ValidationError(errors)

        return value


    # def validate(self, data):
    #     if True in [char.isdigit() for char in data['content']]:
    #         raise serializers.ValidationError("Post cannot have a number in it")
    #     return data

class CommentsOnAPostSerializer(PostsSerializer):
    comments = serializers.SlugRelatedField(many=True, read_only=True, slug_field='content')
    comments_count = serializers.SerializerMethodField()
    class Meta:
        model = Posts
        fields = ["comments","comments_count"]

    def get_comments_count(self, obj):
        return obj.comments.count()
    