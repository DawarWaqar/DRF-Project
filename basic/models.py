from django.db import models

# Create your models here.
class Posts(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    #used textField instead of charfield so that we do not have to specify the max char length here

    
    # class Meta:
    #     ordering = ['created']
    
    


class Comments(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    content = models.CharField(max_length=100)
    post = models.ForeignKey(Posts, on_delete=models.CASCADE, related_name="comments")
    # class Meta:
    #     ordering = ['created']
    
    # def __str__(self):
    #     return self.content