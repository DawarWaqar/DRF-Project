from django.core.cache import cache
from celery import shared_task

@shared_task
def flushAllKeys(globPattern):
    cache.delete_pattern(globPattern)