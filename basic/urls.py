from django.urls import path
from basic import views
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_headers

urlpatterns = [
    path('posts/', cache_page(10*60, key_prefix="POSTS_LIST")(vary_on_headers('User-Agent')(views.PostsList.as_view()))),
    # path('posts/', views.PostsList.as_view()),
    path('posts/<int:pk>/comments/', views.CommentOnAPostCreateView.as_view()),
    path('posts/<int:pk>/', views.PostsDetail.as_view()),
    path('comments/', views.CommentsList.as_view()),
    path('comments/<int:pk>/', views.CommentsDetail.as_view()),
]