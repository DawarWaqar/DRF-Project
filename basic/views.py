from basic.models import Posts, Comments
from basic.serializers import PostsSerializer, CommentsSerializer, CommentsOnAPostSerializer
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import renderers
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from rest_framework import filters
#from django.utils.decorators import method_decorator
#from django.views.decorators.cache import cache_page
#from django.views.decorators.vary import vary_on_headers

class PostsList(generics.ListCreateAPIView):
    queryset = Posts.objects.all()
    serializer_class = PostsSerializer
    ordering=['-created']
    ordering_fields = ['created', ]
    filter_backends = [filters.OrderingFilter]

    #@method_decorator(cache_page(1*60, key_prefix="POSTS_LIST"))
    #@vary_on_headers('User-Agent')
    # def get(self, request, *args, **kwargs):
    #     return super(PostsList, self).get(request, *args, **kwargs)

class PostsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Posts.objects.all()
    serializer_class = PostsSerializer    

class CommentsList(generics.ListCreateAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializer

class CommentsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializer


class CommentsOnAPostList(generics.RetrieveAPIView):
    queryset = Posts.objects.all()
    serializer_class = CommentsOnAPostSerializer

    

class CommentsOnAPostCreate(generics.CreateAPIView):
    

    queryset = Comments.objects.all()

    serializer_class = CommentsSerializer
    
    def perform_create(self, serializer):
        serializer.save(post=get_object_or_404(Posts, id=self.kwargs['pk']))



class BaseCategoryView(APIView):
    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self, 'VIEWS'):
            raise Exception('VIEWS dict should be defined in a derived class')

        if request.method in self.VIEWS:
            return self.VIEWS[request.method]()(request, *args, **kwargs)

        return Response(status=405)


class CommentOnAPostCreateView(BaseCategoryView):
    VIEWS = {
        'GET': CommentsOnAPostList.as_view,
        'POST': CommentsOnAPostCreate.as_view,
    }