from basic.models import Posts
from django.db.models.signals import post_save
from django.dispatch import receiver
from basic.tasks import flushAllKeys

@receiver(post_save, sender=Posts)
def my_callback(sender, **kwargs):
    flushAllKeys.delay("*POSTS_LIST*")